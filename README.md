# Foundations of Concurrent and Distributed Systems Lab: Summer semester 2016 #

This repository contains 5 programming tasks, with their descriptions, sequential C sources, and test inputs. The tasks are taken from the 7th Marathon of Parallel Programming WSCAD-SSC/SBAC-PAD-2012.

# Contributors #

Dmitrii Kuvaiskii <dmitrii.kuvaiskii@tu-dresden.de>

# List of Participants

* Rafael Campos -- Scala [DROPPED OUT]
* Riccardo Steffan -- Java
* Sumanth Korikkar and Cong Lian -- C++ (pthreads/boost) [Internship/part of FCDS module??]
* Mengying Xiong and Yi Zhang -- C
* Felipe de Sousa Silva (S8465986) and Mariya Blavachinskaya -- Java
* Nurbakyt Zhortabayev and Vlad Shkola -- Java
* Oliver Winke and Jan Bickel -- C++ [Grade for Diploma??]
* Ganna Babeniuk and Oleksandr Donchenko -- GPU Cuda / C OpenMP
* Nico Westerbeck -- Rust [Grade for Diploma??]
* Dmytro Astakhov & Severin Simko -- Java